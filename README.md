<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Project installation

In this file I will explain how to install and run the project, the steps are this: 


- Clone project in your local environment
- Create a file .env
- Run artisan command php artisan key:generate , this will create your laravel key
- Run artisan command php artisan migrate , this will populate your database (previously you will have to create a blank database and register in .env file those credfentials)
- After you must run php artisan jwt:secret , this will update your .env file with something like JWT_SECRET=foobar

## Packages

This project uses packages like

- [JWT](https://jwt-auth.readthedocs.io/en/develop/).
- [Laravel CORS](https://github.com/barryvdh/laravel-cors).


## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
