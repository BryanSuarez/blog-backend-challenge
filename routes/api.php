<?php


//register users
Route::post('/register','AuthController@registerUser');
Route::post('/login','AuthController@login');
Route::get('/user','AuthController@me');
Route::post('/logout','AuthController@logout');

Route::group(['prefix' => 'categories'], function(){
    Route::get('/','CategoryController@index');
    Route::post('/','CategoryController@store')->middleware('auth:api');
    Route::get('/{category}','CategoryController@show');
    Route::patch('/{category}','CategoryController@update')->middleware('auth:api');
    Route::delete('/{category}','CategoryController@destroy')->middleware('auth:api');
});

Route::group(['prefix' => 'entries'], function(){
    Route::get('/','PostController@index');
    Route::post('/','PostController@store')->middleware('auth:api');
    Route::get('/{post}','PostController@show');
    Route::patch('/{category}','PostController@update')->middleware('auth:api');
    Route::delete('/{category}','PostController@destroy')->middleware('auth:api');
});

