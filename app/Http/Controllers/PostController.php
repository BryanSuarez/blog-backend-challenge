<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Http\Resources\PostResource;
use Illuminate\Http\Request;
use App\Post;
use App\User;
use Auth;

class PostController extends Controller
{
    public function index()
    {
        $categories = Post::latestFirst()->paginate(5);

        return PostResource::collection($categories);
    }

    public function store(CreatePostRequest $request)
    {
        $post = new Post;
        $post->title = $request->title;
        $post->body = $request->body;
        $post->category_id = $request->category_id;
        $post->user_id = Auth::id();

        $post->save();

        return new PostResource($post);
    }

    public function show(Post $post)
    {
        return new PostResource($post);
    }

    public function update(UpdatePostRequest $request, Post $post)
    {
        $this->authorize('updatePost', $post);
        $post->title = $request->get('title',$post->title);
        $post->body = $request->get('body',$post->body);
        $post->category_id = $request->get('category_id',$post->category_id);
        $post->update();
        return new PostResource($post);
    }

    public function destroy(Request $request,  Post $post)
    {
        $this->authorize('destroyPost', $post);
        $post->delete();
        return response()->json([
            'msg' => 'Post deleted'
        ], 200);
    }
}
