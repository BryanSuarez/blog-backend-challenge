<?php

namespace App\Http\Controllers;

use App\Http\Resources\CategoryResource;
use Illuminate\Http\Request;
 use App\Category;
 use App\User;
 use Auth;

class CategoryController extends Controller
{

    public function index()
    {
        $categories = Category::latestFirst()->get();

        return CategoryResource::collection($categories);
    }

    public function store(Request $request)
    {
        $category = new Category;
        $category->name = $request->name;
        $category->user_id = Auth::id();

        $category->save();

        return new CategoryResource($category);
    }

    public function show(Category $category)
    {
        return new CategoryResource($category);
    }

    public function update(Request $request, Category $category)
    {
        $this->authorize('update', $category);
        $category->name = $request->get('name',$category->title);
        $category->update();
        return new CategoryResource($category);
    }

    public function destroy(Request $request,  Category $category)
    {
        $this->authorize('destroy', $category);
        $category->delete();
        return response()->json([
            'msg' => 'Category deleted'
        ], 200);
    }
}
