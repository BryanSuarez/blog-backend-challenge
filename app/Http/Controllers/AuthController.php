<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginUserRequest;
use App\Http\Requests\RegisterUserRequest;
use http\Env\Response;
use Illuminate\Http\Request;
use App\User;
use App\Http\Resources\UserResource;

class AuthController extends Controller
{
    public function registerUser(RegisterUserRequest $request)
    {
        $user = User::create([
            'email' => $request->email,
            'name' => $request->name,
            'password' => bcrypt($request->password)
        ]);

        if (!$token = auth()->attempt($request->only('email','password'))){
            return abort(401);
        }

        return (new UserResource($request->user()))->additional([
            'meta' => [
                'token' => $token
            ]
        ]);
    }

    public function login(LoginUserRequest $request)
    {
        if (!$token = auth()->attempt($request->only('email','password'))){
            return response()->json([
                'errors' => [
                    'email' => 'Whoops, we can not find an user with those credentials.'
                ]
            ],422);
        }

        return (new UserResource($request->user()))->additional([
            'meta' => [
                'token' => $token
            ]
        ]);
    }

    public function me(Request $request)
    {
        return new UserResource($request->user());
    }

    public function logout()
    {
        auth()->logout();
    }
}
