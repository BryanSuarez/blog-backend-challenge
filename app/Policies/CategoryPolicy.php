<?php

namespace App\Policies;

use App\User;
use App\Category;
use Illuminate\Auth\Access\HandlesAuthorization;

class CategoryPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function update(User $user, Category $category)
    {
        return $user->categoryCreator($category);
    }

    public function destroy(User $user, Category $category)
    {
        return $user->categoryCreator($category);
    }
}
